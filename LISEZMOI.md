# LISEZMOI #


## Avant le test

1. Cloner le répertoire. 
1. Checkout la branche qui vous a été donnée

# Test
Important: Les questions sont écrites dans une syntaxe Java. Par contre, vous n'êtes pas obligé d'utiliser Java. Vous pouvez utiliser le langage orienté-objet de votre choix ou du pseudo-code. 

Chacune des questions se trouvent dans un dossier. Veuillez répondre dans le LISEZMOI du dossier sauf indication contraire. 

Vous avez 1 heure pour compléter ce test. Tout est permis, incluant internet. 