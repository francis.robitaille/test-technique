package com.arcbees.technicaltest;

import java.time.OffsetDateTime;

import com.arcbees.technicaltest.doNotEdit.*;

@Component
public class FlightService {

    private final FlightRepository flightRepository;
    private final AirportAnnouncer announcer;

    @Autowired
    public FlightService(FlightRepository flightRepository, AirportAnnouncer announcer) {
        this.flightRepository = flightRepository;
        this.announcer = announcer;
    }

    public void delayFlight(Flight flight, OffsetDateTime newTime) {
        flight.delay(newTime);

        this.flightRepository.persist(flight);

        if (flight.isDeparture()) {
            this.announcer.announceTimeChanged(flight);
        }
    }
}
