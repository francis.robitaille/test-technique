package com.arcbees.technicaltest;

import java.time.OffsetDateTime;

import com.arcbees.technicaltest.doNotEdit.*;

public class FlightServiceTest extends JasmineTest implements WithAssertions

    // Supposons que nous utilisons jasmine :)

    private static final OffsetDateTime ANY_DATE_TIME = OffsetDateTime.now();

    @Mock
    private FlightRepository flightRepository;
    @Mock
    private AirportAnnouncer announcer;

    @InjectMocks
    private FlightService flightService;

    private Flight flight;

    @Override
    protected void init() {
        describe("testing FlightService", () -> {

            describe("method delayFlight", () -> {

                describe("with a departure flight", () -> {

                    beforeEach(() -> {
                        this.flight = new Flight();
                        this.flight.setIsDeparture(true)

                        flightService.delayFlight(this.flight, ANY_DATE_TIME);
                    });

                    it("flight delay is set", () -> {
                        assertThat(this.flight.getDelay).equals(ANY_DATE_TIME);
                    });

                    it("flight is persisted", () -> {
                        verify(flightRepository)).persist(any(Flight.class));
                    });

                    it("announcer has annonced time changed", () -> {
                        verify(announcer)).announceTimeChanged(any(Flight.class));
                    });
                });

                describe("with a non departure flight", () -> {

                    beforeEach(() -> {
                        this.flight = new Flight();
                        this.flight.setIsDeparture(false)

                        flightService.delayFlight(this.flight, ANY_DATE_TIME);
                    });

                    it("flight delay is set", () -> {
                        assertThat(this.flight.getDelay).equals(ANY_DATE_TIME);
                    });

                    it("flight is persisted", () -> {
                        verify(flightRepository)).persist(any(Flight.class));
                    });

                    it("announcer has not annonced time changed", () -> {
                        verifyZeroInteractions(announcer);
                    });
                });
            });
        });
    };

    private void explication_cycle_tdd(){
        Le tdd (Test-Driven Development) est une pratique qui qui consiste à écrire chacun des tests avant d'avoir fait
        la fonctionnalité. Voici les étapes du tdd:

        La première étape est d'analyser la fonctionnality à faire pour ensuite écrire un test correspondant à celle-ci.
        Deuxièmement, lancer le test et s'assurer qu'il échoue étant donné que la fonctionnalité n'est aps faite.
        Troisièmement, écrire la fonctionnalité en fonction du test écrit (ça aide à structurer son code)
        Quatrièmement, lancer les tests et s'assurer qu'ils passent tous
        Finalement, revalider que les test complète bien la fonctionnalité et l'ajuster au besoin.
    }
}
