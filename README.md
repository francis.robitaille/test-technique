# README #

This repository is the technical test for a developer. 


## Before the test

1. Clone this repository.
1. Checkout the branch that was given to you


# Test
Important: Questions are often written in java syntax, but this does not mean you need to use Java. Use any object oriented language you want, even pseudo-code.

Each question is in its folder. Please answer directly in the README unless specified otherwise. 

You have 1 hour to complete this test. You can use everything you want, including internet. 