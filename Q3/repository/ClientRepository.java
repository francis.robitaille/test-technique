

public interface ClientRepository extends JpaRepository<Client, String> {
    Client findByEmail(String email);
}
