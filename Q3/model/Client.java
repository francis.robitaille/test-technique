@Entity
@Data
public class Client {

    @Id
    private String email;
    private String name;
    private Plan plan;
}