@Entity
@Data
public class Plan {

    @Id
    private String name;

    isValid() {
        return this.name == EPlan.SILVER || this.name == EPlan.GOLD || this.name == EPlan.PLATINUM;
    }
}