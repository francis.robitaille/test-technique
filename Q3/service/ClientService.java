@Component
public class ClientService {

    private final ClientRepository clientRepository;

    public ClientService(ClientRepository clientRepository) {
        this.clientRepository = clientRepository;
    }

    public Client getClient(String email) {
        return clientRepository.findByEmail(email);
    }

    public boolean clientAlreadyExist(String email) {
        return getClient(email) != null;
    }

    public ResponseEntity addClient(Client client) {
        if(clientAlreadyExist(client.getEmail())) {
            return new ResponseEntity("Client already exist", HttpStatus.BAD_REQUEST);
        } else {
            this.clientRepository.save(client);
            return new ResponseEntity(client, HttpStatus.OK);
        }
    }

    public ResponseEntity registerPlan(Client client, Plan plan) {
        if(plan.isValid()) {
            client.setPlan(plan);
            this.clientRepository.save(client);
            return new ResponseEntity(client, HttpStatus.OK);
        }else {
            return new ResponseEntity("Invalid plan", HttpStatus.BAD_REQUEST);
        }
    }

    public ResponseEntity unregisterPlan(Client client) {
        if(plan.isValid()) {
            client.setPlan(nullp);
            this.clientRepository.save(client);
            return new ResponseEntity(client, HttpStatus.OK);
        } else {
            return new ResponseEntity("Invalid plan", HttpStatus.BAD_REQUEST);
        }
    }
}