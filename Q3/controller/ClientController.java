

@RestController
@RequestMapping("/clients")
public class ClientController {

    private final ClientService clientService;

    @Autowired
    public ClientController(ClientService clientService) {
        this.clientService = clientService;
    }

    @GetMapping(value = "/{email}")
    public ResponseEntity getByEmail(@PathVariable String email) {
        Client client = clientService.getClient(email);
        if(client == null) {
            return new ResponseEntity(String.format("No client found for email %s", email), HttpStatus.NOT_FOUND);
        } else {
            return new ResponseEntity(client, HttpStatus.OK);
        }
    }

    @PostMapping(value = "/register")
    public ResponseEntity add(@RequestBody Client client) {
        return this.clientService.addClient(client);
    }

    // ******************************************
    // Je crois qu'en temps normal, j'aurais splitter  le register en dehors du client, mais je vais manquer de temps... :/
    // ******************************************

    @GetMapping(value = "/{email}/registerplan")
    public ResponseEntity registerPlan(@PathVariable String email, @RequestParam("plan") String plan) {
        Client client = clientService.getClient(email);
        if(client == null) {
            return new ResponseEntity(String.format("No client found for email %s", email), HttpStatus.NOT_FOUND);
        } else {
            this.clientService.registerplan(client, plan);
            return new ResponseEntity(client, HttpStatus.OK);
        }
    }

    @GetMapping(value = "/{email}/unregisterplan")
    public ResponseEntity unregisterPlan(@PathVariable String email) {
        Client client = clientService.getClient(email);
        if(client == null) {
            return new ResponseEntity(String.format("No client found for email %s", email), HttpStatus.NOT_FOUND);
        } else {
            this.clientService.unregisterplan(client);
            return new ResponseEntity(client, HttpStatus.OK);
        }
    }
}