public class ResponseBodyWriter {

    private static final String XML_CONTENT_TYPE = "application/xml";
    private static final String JSON_CONTENT_TYPE = "application/json";

    private final Serializer m_srlz;

    public ResponseBodyWriter(Serializer s) {
        this.m_srlz = s;
    }

    public void write(Response r, Object c) {
        ContentType contentType = r.getContentType();
        String content = null;

        switch(contentType) {
            case XML_CONTENT_TYPE:
                content = m_srlz.writeXml(c);
                break;
            case JSON_CONTENT_TYPE:
                content = m_srlz.writeJson(c);
                break;
            default:
                content = c.toString();
                break;
        }
        r.getWriter().write(content);
    }
}
