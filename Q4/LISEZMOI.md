## Question 4 (Git)

### A

Quels sont les différences entre un _merge_ et un _rebase_? Expliquez comment il est possible de perdre du code avec les deux méthodes. 

----------------------------------------------------------------------------------------

Merge prend tout les changements d'une branche pour les verser dans une autre pour former qu'une branche.

Alors que Rebase permet en quelque sorte de faire de même, mais qui permet de modifier l'historique des commit. Il a également la possibilité de faire des rebase intéractif afin de picorer certain commit voulus pour finalement qu'en faire 1.

Ça m'est en effet déja arrivé de perdre du code avec un rebase intérectif. Il y avait des complits et j'ai malheureusement selectionnais les mauvais commit. Lorsque le rebase fut finit, il était impossible de revenir à l'arrière.

### B 


Quand vous avez fini de répondre aux questions précédentes, commitez vos modifications en utilisant un message de commit significatif. Avant de pousser votre code, _rebasez_-vous sur master. Ensuite, poussez votre nouveau code sur la branche. 

